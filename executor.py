import string, os

keycodes = {
	'q':0x10, 'w':0x11, 'e':0x12, 'r':0x13, 't':0x14, 'y':0x15, 'u':0x16, 'i':0x17, 'o':0x18, 'p':0x19,
	'a':0x1E, 's':0x1F, 'd':0x20, 'f':0x21, 'g':0x22, 'h':0x23, 'j':0x24, 'k':0x25, 'l':0x26,
	'z':0x2C, 'x':0x2D, 'c':0x2E, 'v':0x2F, 'b':0x30, 'n':0x31, 'm':0x32,
	'1':0x02, '2':0x03, '3':0x04, '4':0x05, '5':0x06, '6':0x07, '7':0x08, '8':0x09, '9':0x0A, '0':0x0B,

	'shift':0x2A, 'alt':0x38, 'tab':0x0F, 'space':0x39, 'ctrl':0x1D, 'esc':0x01, 'escape':0x01, 'backspace':0x0E, 'enter':0x1C, 'kappa':0x1C,
	'up':0x48, 'down':0x50, 'left':0x4B, 'right':0x4D, "home":0x47, "end":0x4F,
	'-':0x0C, '=':0x0D, '\'':0x28, ';':0x27, ',':0x33, 'dot':0x34, 'slash':0x35, '`':0x29, '\\':0x2B, '[':0x1A, ']':0x1B,
	'f1':0x3B, 'f2':0x3C, 'f3':0x3D, 'f4':0x3E, 'f5':0x3F, 'f6':0x40, 'f7':0x41, 'f8':0x42, 'f9':0x43, 'f10':0x44, 'f11':0x57, 'f12':0x58
}

numChars = [")", "!", "at", "#", "$", "%", "^", "&", "*", "("]
shiftChars = {'-':'_', '=':'+', '\'':'"', ';':':', ',':'<', 'dot':'>', 'slash':'?', '`':'~', '\\':'|', '[':'{', ']':'}'}

def processMessage (inp):
	if (len (inp) == 1 and (inp in string.ascii_lowercase or inp in string.digits)):
		sendKeyCodes (keycodes [inp])

	elif (len (inp) == 1 and (inp in string.ascii_uppercase)):
		code = keycodes [inp.lower()]
		sendKeyCodes (keycodes ["shift"], code)

	elif (inp[0].lower () == "f" and inp[1:].isdigit ()):
		number = int (inp[1:])

		if (number >= 1 and number <= 12):
			sendKeyCodes (keycodes["f" + str (number)])
		
	elif (inp.startswith ("alt-f") and inp[5:].isdigit()):
		number = int (inp[5:])

		if (number >= 1 and number <= 12):
			code = keycodes["f" + str (number)]
			sendKeyCodes (keycodes ["alt"], code)
		
	elif (inp.startswith ("alt-")):
		inp = inp[4:]

		if (inp in string.ascii_lowercase or inp in ["tab", "space", ";"]):
			sendKeyCodes (keycodes ["alt"], keycodes [inp])
		
	elif (inp.startswith ("ctrl-")):
		inp = inp[5:]

		if (inp in string.ascii_letters):
			sendKeyCodes (keycodes ["ctrl"], keycodes [inp.lower ()])

	elif (inp.startswith ("ctrl-shift-")):
		inp = inp[5:]

		if (inp in string.ascii_letters):
			sendKeyCodes (keycodes ["ctrl"], keycodes ["shift"], keycodes [inp.lower ()])

	elif (inp in ["escape", "esc", "backspace", "enter", "kappa", "space", "tab", "up", "down", "left", "right", "home", "end"]):
		sendKeyCodes (keycodes[inp])

	elif (inp in numChars):
		position = numChars.index (inp)
		sendKeyCodes (keycodes["shift"], keycodes[str (position)])

	elif (inp in shiftChars):
		sendKeyCodes (keycodes[inp])

	elif (inp in shiftChars.values ()):
		initialKey = list (shiftChars.keys ())[list (shiftChars.values ()).index (inp)]
		sendKeyCodes (keycodes["shift"], keycodes [initialKey])

	elif (inp == "startvm"):
		os.system ("VBoxManage startvm Gentoo")

	elif (inp == "poweroff"):
		os.system ("VBoxManage controlvm Gentoo poweroff")

	elif (inp == "acpipowerbutton"):
		os.system ("VBoxManage controlvm Gentoo acpipowerbutton")

def isValidCmd (cmd):
	if (len (cmd) == 0):
		return False

	if (len (cmd) == 1 and cmd in string.printable):
		return True

	elif (cmd[0].lower () == "f" and cmd[1:].isdigit ()):
		return True

	elif (cmd.startswith ("alt-") or cmd.startswith ("ctrl-")):
		return True

	elif (cmd in ["escape", "esc", "backspace", "enter", "kappa", "space", "tab", "up", "down", "left", "right", "home", "end", "nop"]):
		return True

	elif (cmd in ["dot", "slash", "at"]):
		return True

	elif (cmd in ["startvm", "poweroff", "acpipowerbutton"]):
		return False # invalidated til further notice

	else:
		return False

def sendKeyCodes (*keys):
	payload = " ".join (list (map (hexstr, keys)) + list (map (lambda c : hexstr(c + 0x80), reversed (keys))))
	os.system ("VBoxManage controlvm Gentoo keyboardputscancode " + payload)

def hexstr (code):
	return hex (code)[2:].zfill (2)