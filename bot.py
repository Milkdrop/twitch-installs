import time, socket, threading, json
import logging
from flask import Flask
from operator import itemgetter
from executor import *

log = logging.getLogger ('werkzeug')
log.setLevel (logging.ERROR)

votes = {}
voteInterval = 8
lastExecTime = time.time ()

app = Flask (__name__)

@app.route("/")
def getOverlayHTML ():
	return overlay

@app.route("/getData")
def getData():
	timeLeft = lastExecTime + voteInterval - time.time ()
	if (timeLeft <= 0):
		timeLeft = 0
	else:
		timeLeft = round (timeLeft, 1)

	cmdList = getSortedCmdList (votes)
	return json.dumps ({"cmdList": cmdList, "timeLeft": round (timeLeft, 1)})

def parseVotes ():
	global lastExecTime

	while True:
		try:
			time.sleep (voteInterval)
			lastExecTime = time.time ()

			cmds = getSortedCmdList (votes)
			votes.clear ()

			if (len (cmds) == 0):
				continue

			cmd = cmds[0][0]

			if (cmd != "nop"):
				processMessage (cmd)

		except Exception as e:
			print (e)

def getSortedCmdList (votes):
	cmds = {}
	
	for author in votes:
		cmd = votes[author]
		if (cmd not in cmds):
			cmds[cmd] = 1
		else:
			cmds[cmd] += 1
	
	return sorted (cmds.items(), key = itemgetter (1))[::-1]

def rebuildSocket ():
	global sock

	try:
		sock.close ()
	except:
		pass

	sock = socket.socket ()
	sock.settimeout (60 * 5)
	sock.connect (('irc.chat.twitch.tv', 6667))

	sock.send ("PASS {}\n".format (token).encode ())
	sock.send (b"NICK bot\n")
	sock.send (b"JOIN #htspctf\n")

def getPath ():
	return '/'.join (__file__.split ('/')[:-1])

teamfp = open (getPath () + "/teamList.txt", "a+")
token = open (getPath () + "/token", "r").read ().strip ()
rebuildSocket ()

executionThread = threading.Thread (target = parseVotes)
executionThread.daemon = True
executionThread.start ()

serverThread = threading.Thread (target = app.run, args = ("0.0.0.0", 8080))
serverThread.daemon = True
serverThread.start ()

overlay = open (getPath () + "/overlay.html", "r").read ()

while True:
	try:
		resp = sock.recv (2048).decode ()

		if resp.startswith ('PING'):
			print ("ping")
			sock.send ("PONG\n".encode ())

		elif len (resp) > 0:
			resp = resp.strip ()

			author = resp[1:]
			author = author[:author.find ("!")]
			msg = ":".join (resp.split (":")[2:])
			
			if (msg[:5] == "!team"):
				print (author + ": " + msg)
				teamfp.write (author + ": " + msg + "\n")
				teamfp.flush ()

			if (isValidCmd (msg)):
				votes[author] = msg
			else:
				if (msg[0] == '!'):
					msg = msg[1:]
					if (isValidCmd (msg)):
						votes[author] = msg

	except socket.timeout:
		rebuildSocket ()

	except Exception as e:
		print (e)